﻿namespace ExcelMacrosExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Calc = new System.Windows.Forms.Button();
            this.textBox_Thickness1 = new System.Windows.Forms.TextBox();
            this.textBox_Thickness2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_Cost2 = new System.Windows.Forms.TextBox();
            this.textBox_Cost1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_TicknessMax = new System.Windows.Forms.TextBox();
            this.textBox_TicknessMin = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_Calc
            // 
            this.button_Calc.Location = new System.Drawing.Point(12, 146);
            this.button_Calc.Name = "button_Calc";
            this.button_Calc.Size = new System.Drawing.Size(75, 23);
            this.button_Calc.TabIndex = 0;
            this.button_Calc.Text = "Рассчитать";
            this.button_Calc.UseVisualStyleBackColor = true;
            this.button_Calc.Click += new System.EventHandler(this.button_Calc_Click);
            // 
            // textBox_Thickness1
            // 
            this.textBox_Thickness1.Location = new System.Drawing.Point(110, 57);
            this.textBox_Thickness1.Name = "textBox_Thickness1";
            this.textBox_Thickness1.Size = new System.Drawing.Size(100, 20);
            this.textBox_Thickness1.TabIndex = 1;
            this.textBox_Thickness1.Text = "1";
            // 
            // textBox_Thickness2
            // 
            this.textBox_Thickness2.Location = new System.Drawing.Point(110, 96);
            this.textBox_Thickness2.Name = "textBox_Thickness2";
            this.textBox_Thickness2.Size = new System.Drawing.Size(100, 20);
            this.textBox_Thickness2.TabIndex = 2;
            this.textBox_Thickness2.Text = "2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Материал 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Материал 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Толщина";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Стоимость";
            // 
            // textBox_Cost2
            // 
            this.textBox_Cost2.Location = new System.Drawing.Point(222, 96);
            this.textBox_Cost2.Name = "textBox_Cost2";
            this.textBox_Cost2.Size = new System.Drawing.Size(100, 20);
            this.textBox_Cost2.TabIndex = 7;
            this.textBox_Cost2.Text = "2";
            // 
            // textBox_Cost1
            // 
            this.textBox_Cost1.Location = new System.Drawing.Point(222, 57);
            this.textBox_Cost1.Name = "textBox_Cost1";
            this.textBox_Cost1.Size = new System.Drawing.Size(100, 20);
            this.textBox_Cost1.TabIndex = 6;
            this.textBox_Cost1.Text = "3";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox_Thickness1);
            this.groupBox1.Controls.Add(this.textBox_Cost2);
            this.groupBox1.Controls.Add(this.textBox_Thickness2);
            this.groupBox1.Controls.Add(this.textBox_Cost1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 128);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Исходные данные";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_TicknessMin);
            this.groupBox2.Controls.Add(this.textBox_TicknessMax);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(355, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 128);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ограничения";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Минимальная толщина";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Максимальная толщина";
            // 
            // textBox_TicknessMax
            // 
            this.textBox_TicknessMax.Location = new System.Drawing.Point(9, 96);
            this.textBox_TicknessMax.Name = "textBox_TicknessMax";
            this.textBox_TicknessMax.Size = new System.Drawing.Size(100, 20);
            this.textBox_TicknessMax.TabIndex = 11;
            this.textBox_TicknessMax.Text = "3";
            // 
            // textBox_TicknessMin
            // 
            this.textBox_TicknessMin.Location = new System.Drawing.Point(9, 41);
            this.textBox_TicknessMin.Name = "textBox_TicknessMin";
            this.textBox_TicknessMin.Size = new System.Drawing.Size(100, 20);
            this.textBox_TicknessMin.TabIndex = 11;
            this.textBox_TicknessMin.Text = "2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 215);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button_Calc);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Calc;
        private System.Windows.Forms.TextBox textBox_Thickness1;
        private System.Windows.Forms.TextBox textBox_Thickness2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_Cost2;
        private System.Windows.Forms.TextBox textBox_Cost1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox_TicknessMin;
        private System.Windows.Forms.TextBox textBox_TicknessMax;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}

