﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelMacrosExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Calc_Click(object sender, EventArgs e)
        {
            Excel.Application objExcel = null;
            Excel.Workbook WorkBook = null;

            try
            {
                objExcel = new Excel.Application();

                //если надо показать Excel-файл
                //objExcel.ScreenUpdating = true;
                //objExcel.WindowState = Excel.XlWindowState.xlMaximized;
                //objExcel.Visible = true;
                //objExcel.DisplayAlerts = true;

                string fileName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Book.xlsm");
                
                WorkBook = objExcel.Workbooks.Open(fileName, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                Excel.Worksheet WorkSheet = (Excel.Worksheet)WorkBook.Sheets["Data"];
                WorkSheet.Range["C4"].Value2 = Convert.ToDouble(textBox_Thickness1.Text);
                WorkSheet.Range["C5"].Value2 = Convert.ToDouble(textBox_Thickness2.Text);
                WorkSheet.Range["D4"].Value2 = Convert.ToDouble(textBox_Cost1.Text);
                WorkSheet.Range["D5"].Value2 = Convert.ToDouble(textBox_Cost2.Text);
                WorkSheet.Range["C8"].Value2 = Convert.ToDouble(textBox_TicknessMin.Text);
                WorkSheet.Range["C9"].Value2 = Convert.ToDouble(textBox_TicknessMax.Text);

                objExcel.GetType().InvokeMember("Run", BindingFlags.Default | BindingFlags.InvokeMethod,
                    null, objExcel, new Object[] { "SearchCalc" });

                textBox_Thickness1.Text = WorkSheet.Range["C4"].Value.ToString("0.##");
                textBox_Thickness2.Text = WorkSheet.Range["C5"].Value.ToString("0.##");

                MessageBox.Show("Решение найдено.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка вызова (" + ex.Message + ").");
            }
            finally
            {
                if (WorkBook != null) WorkBook.Close(false, null, null);
                if (objExcel != null) objExcel.Quit();
            }
        }
    }
}
